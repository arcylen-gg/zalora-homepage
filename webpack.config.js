const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
    devtool: 'eval-source-map',
    entry: './src/app.ts',
    output: {
        // publicPath: 'public',
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    module: {
        rules: [
            {
                test: /\.ts$/, // if the test passes
                use: 'ts-loader', // it will use this the package we installed to compile
                exclude: /node_modules/,
                include: [path.resolve(__dirname, 'src')] // inside the `src/app`
            },
            {
                test: /\.scss$/,
                use: [
                  // Creates `style` nodes from JS strings
                  'style-loader',
                  // Translates CSS into CommonJS
                  'css-loader',
                  'resolve-url-loader',
                  // Compiles Sass to CSS
                  {
                    loader: 'sass-loader',
                    options: {
                        implementation: require('sass'),
                        sassOptions: {
                          fiber: false,
                        },
                    },
                  },
                ],
              },
        ]
    },
    resolve: {
        extensions: ['.ts','.js', '.scss']// So webpack know what type are we goingt to import
    },
    plugins: [
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: '[name].css',
        chunkFilename: '[id].css',
      }),
    ],
}